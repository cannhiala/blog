@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($asset1->name) ? $asset1->name : 'Asset1' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('asset1s.asset1.destroy', $asset1->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('asset1s.asset1.index') }}" class="btn btn-primary" title="Show All Asset1">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('asset1s.asset1.create') }}" class="btn btn-success" title="Create New Asset1">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('asset1s.asset1.edit', $asset1->id ) }}" class="btn btn-primary" title="Edit Asset1">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Asset1" onclick="return confirm(&quot;Click Ok to delete Asset1.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Name</dt>
            <dd>{{ $asset1->name }}</dd>
            <dt>Description</dt>
            <dd>{{ $asset1->description }}</dd>
            <dt>Category</dt>
            <dd>{{ optional($asset1->category)->id }}</dd>

        </dl>

    </div>
</div>

@endsection