<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix' => 'asset_categories',
], function () {
    Route::get('/', 'App\Http\Controllers\AssetCategoriesController@index')
         ->name('asset_categories.asset_category.index');
    Route::get('/create','App\Http\Controllers\AssetCategoriesController@create')
         ->name('asset_categories.asset_category.create');
    Route::get('/show/{assetCategory}','App\Http\Controllers\AssetCategoriesController@show')
         ->name('asset_categories.asset_category.show')->where('id', '[0-9]+');
    Route::get('/{assetCategory}/edit','App\Http\Controllers\AssetCategoriesController@edit')
         ->name('asset_categories.asset_category.edit')->where('id', '[0-9]+');
    Route::post('/', 'App\Http\Controllers\AssetCategoriesController@store')
         ->name('asset_categories.asset_category.store');
    Route::put('asset_category/{assetCategory}', 'App\Http\Controllers\AssetCategoriesController@update')
         ->name('asset_categories.asset_category.update')->where('id', '[0-9]+');
    Route::delete('/asset_category/{assetCategory}','App\Http\Controllers\AssetCategoriesController@destroy')
         ->name('asset_categories.asset_category.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'asset1s',
], function () {
    Route::get('/', 'App\Http\Controllers\Asset1sController@index')
         ->name('asset1s.asset1.index');
    Route::get('/create','App\Http\Controllers\Asset1sController@create')
         ->name('asset1s.asset1.create');
    Route::get('/show/{asset1}','App\Http\Controllers\Asset1sController@show')
         ->name('asset1s.asset1.show')->where('id', '[0-9]+');
    Route::get('/{asset1}/edit','App\Http\Controllers\Asset1sController@edit')
         ->name('asset1s.asset1.edit')->where('id', '[0-9]+');
    Route::post('/', 'App\Http\Controllers\Asset1sController@store')
         ->name('asset1s.asset1.store');
    Route::put('asset1/{asset1}', 'App\Http\Controllers\Asset1sController@update')
         ->name('asset1s.asset1.update')->where('id', '[0-9]+');
    Route::delete('/asset1/{asset1}','App\Http\Controllers\Asset1sController@destroy')
         ->name('asset1s.asset1.destroy')->where('id', '[0-9]+');
});
