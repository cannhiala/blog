<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Asset1 extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'asset1s';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'name',
                  'description',
                  'category_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the category for this model.
     *
     * @return App\Models\AssetCategory
     */
    public function category()
    {
        return $this->belongsTo('App\Models\AssetCategory','category_id');
    }



}
