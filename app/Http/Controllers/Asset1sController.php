<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Asset1;
use App\Models\AssetCategory;
use Illuminate\Http\Request;
use Exception;

class Asset1sController extends Controller
{

    /**
     * Display a listing of the asset1s.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $asset1s = Asset1::with('category')->paginate(25);

        return view('asset1s.index', compact('asset1s'));
    }

    /**
     * Show the form for creating a new asset1.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $categories = AssetCategory::pluck('id','id')->all();
        
        return view('asset1s.create', compact('categories'));
    }

    /**
     * Store a new asset1 in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            Asset1::create($data);

            return redirect()->route('asset1s.asset1.index')
                ->with('success_message', 'Asset1 was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified asset1.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $asset1 = Asset1::with('category')->findOrFail($id);

        return view('asset1s.show', compact('asset1'));
    }

    /**
     * Show the form for editing the specified asset1.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $asset1 = Asset1::findOrFail($id);
        $categories = AssetCategory::pluck('id','id')->all();

        return view('asset1s.edit', compact('asset1','categories'));
    }

    /**
     * Update the specified asset1 in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $asset1 = Asset1::findOrFail($id);
            $asset1->update($data);

            return redirect()->route('asset1s.asset1.index')
                ->with('success_message', 'Asset1 was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified asset1 from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $asset1 = Asset1::findOrFail($id);
            $asset1->delete();

            return redirect()->route('asset1s.asset1.index')
                ->with('success_message', 'Asset1 was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'name' => 'string|min:1|max:255|nullable',
            'description' => 'string|min:1|max:1000|nullable',
            'category_id' => 'nullable', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
